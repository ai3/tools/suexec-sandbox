#ifndef __suexec_conf_H
#define __suexec_conf_H 1

#include "strlist.h"

struct config {
  char *path;
  char *root;
  strlist allowed_cmds;
  strlist docroots;
  int min_uid;
  int min_gid;
  int enable_capabilities;
  int enable_cgroups;
  int enable_namespaces;
  int enable_syscalls;
};

int s2i(const char *, int *);

int read_config(const char *, struct config *);

#endif
