#ifndef __suexec_sandbox_H
#define __suexec_sandbox_H 1

#include <sys/types.h>

#define HOSTNAME_SIZE 64

struct sandbox_config {
  char hostname[HOSTNAME_SIZE];
  int fd;

  uid_t uid;
  gid_t gid;
  int argc;
  char *argv0;
  char **argv;
  char *new_root_dir;
  int enable_capabilities;
  int enable_cgroups;
  int enable_namespaces;
  int enable_syscalls;
};

int sandbox_config_init(struct sandbox_config *, uid_t, gid_t, int, char *, char **);
int sandbox_start(struct sandbox_config *);

#endif
