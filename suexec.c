/*
 * Simple suexec replacement, customized for A/I hosting needs.
 *
 * It is configurable, and it does not use NSS.
 */

#define _GNU_SOURCE 1

#include "config.h"
#include "conf.h"
#include "log.h"
#include "sandbox.h"

#include <errno.h>
#include <fcntl.h>
#include <grp.h> /* for setgroups() */
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define SUEXEC_CONFIG "/etc/apache2/suexec-sandbox.conf"
#define SUEXEC_CONFIG_DIR "/etc/apache2/suexec-sandbox.d"
#define DEFAULT_SAFE_PATH "/bin:/usr/bin"

#define MAX_ENV_SIZE 256

static const char *const safe_env_lst[] = {
    /* variable name starts with */
  "HTTP_", "SSL_",

  "PHPRC=",

    /* variable name is */
    "AUTH_TYPE=", "CONTENT_LENGTH=", "CONTENT_TYPE=", "CONTEXT_DOCUMENT_ROOT=",
    "CONTEXT_PREFIX=", "DATE_GMT=", "DATE_LOCAL=", "DOCUMENT_ARGS=",
    "DOCUMENT_NAME=", "DOCUMENT_PATH_INFO=", "DOCUMENT_ROOT=", "DOCUMENT_URI=",
    "GATEWAY_INTERFACE=", "HTTPS=", "LAST_MODIFIED=", "PATH_INFO=",
    "PATH_TRANSLATED=", "QUERY_STRING=", "QUERY_STRING_UNESCAPED=",
    "REMOTE_ADDR=", "REMOTE_HOST=", "REMOTE_IDENT=", "REMOTE_PORT=",
    "REMOTE_USER=", "REDIRECT_ERROR_NOTES=", "REDIRECT_HANDLER=",
    "REDIRECT_QUERY_STRING=", "REDIRECT_REMOTE_USER=",
    "REDIRECT_SCRIPT_FILENAME=", "REDIRECT_STATUS=", "REDIRECT_URL=",
    "REQUEST_METHOD=", "REQUEST_URI=", "REQUEST_SCHEME=", "SCRIPT_FILENAME=",
    "SCRIPT_NAME=", "SCRIPT_URI=", "SCRIPT_URL=", "SERVER_ADMIN=",
    "SERVER_NAME=", "SERVER_ADDR=", "SERVER_PORT=", "SERVER_PROTOCOL=",
    "SERVER_SIGNATURE=", "SERVER_SOFTWARE=", "UNIQUE_ID=", "USER_NAME=", "TZ=",
    NULL};

extern char **environ;

static char **saved_env;

static void save_env() {
  /* Wipe the environment as soon as possible, to prevent libc function side
   * effects. */
  static char *empty_ptr = NULL;
  saved_env = environ;
  environ = &empty_ptr;
}

static int clear_env(struct config *config) {
  char pathbuf[512];
  char **cleanenv;
  char **ep;
  int cidx = 0;
  int idx;

  /* While cleaning the environment, the environment should be clean.
   * (e.g. malloc() may get the name of a file for writing debugging info.
   * Bad news if MALLOC_DEBUG_FILE is set to /etc/passwd.  Sprintf() may be
   * susceptible to bad locale settings....)
   * (from PR 2790)
   */
  char **envp = saved_env;

  if ((cleanenv = (char **)calloc(MAX_ENV_SIZE, sizeof(char *))) == NULL) {
    log_println("out of memory (allocating environment)");
    return -1;
  }

  sprintf(pathbuf, "PATH=%s", config->path ? config->path : DEFAULT_SAFE_PATH);
  cleanenv[cidx] = strdup(pathbuf);
  if (cleanenv[cidx] == NULL) {
    log_println("out of memory (allocating environment)");
    return -1;
  }
  cidx++;

  for (ep = envp; *ep && cidx < MAX_ENV_SIZE - 1; ep++) {
    for (idx = 0; safe_env_lst[idx]; idx++) {
      if (!strncmp(*ep, safe_env_lst[idx], strlen(safe_env_lst[idx]))) {
        cleanenv[cidx] = *ep;
        cidx++;
        break;
      }
    }
  }

  cleanenv[cidx] = NULL;

  environ = cleanenv;
  return 0;
}

static int check_cmd(struct config *config, char *cmd) {
  struct stat prg_info;
  int i, allowed = 1;

  // Check if the command is allowed (only if allowed_cmd directives
  // are specified in the configuration).
  if (config->allowed_cmds.sz > 0) {
    allowed = 0;
    for (i = 0; i < config->allowed_cmds.sz; i++) {
      if (!strcmp(cmd, config->allowed_cmds.values[i])) {
        allowed = 1;
        break;
      }
    }
  }
  if (!allowed) {
    log_printf("command not allowed (%s)", cmd);
    return -1;
  }

  // Stat the program.
  if ((lstat(cmd, &prg_info) != 0) || (S_ISLNK(prg_info.st_mode))) {
    log_printf("cannot stat program (%s)", cmd);
    return -1;
  }

  if ((prg_info.st_mode & S_IWOTH) || (prg_info.st_mode & S_IWGRP)) {
    log_printf("program is writable by others (%s)", cmd);
    return -1;
  }

  if ((prg_info.st_mode & S_ISUID) || (prg_info.st_mode & S_ISGID)) {
    log_printf("program is setuid/setgid (%s)", cmd);
    return -1;
  }

  return 0;
}

static int check_cwd(struct config *config) {
  struct stat dir_info;
  int i, allowed = 1;
  char cwd[PATH_MAX];

  if (!getcwd(cwd, sizeof(cwd))) {
    log_println_errno("getcwd failed");
    return -1;
  }

  if ((lstat(cwd, &dir_info) != 0) || (!S_ISDIR(dir_info.st_mode))) {
    log_printf("cannot stat cwd (%s)", cwd);
    return -1;
  }
  if ((dir_info.st_mode & S_IWOTH) || (dir_info.st_mode & S_IWGRP)) {
    log_printf("directory is writable by others (%s)", cwd);
    return -1;
  }

  if (config->docroots.sz > 0) {
    allowed = 0;
    for (i = 0; i < config->docroots.sz; i++) {
      // Docroot must either match identically cwd, or cwd must have
      // (docroot + /) as a prefix.
      int n = strlen(config->docroots.values[i]);
      if (!strcmp(cwd, config->docroots.values[i]) ||
          (!strncmp(cwd, config->docroots.values[i], n) && cwd[n] == '/')) {
        allowed = 1;
        break;
      }
    }
  }
  if (!allowed) {
    log_printf("docroot not allowed (%s)", cwd);
    return -1;
  }

  return 0;
}

int main(int argc, char **argv) {
  int target_uid;
  int target_gid;
  char *cmd, *real_cmd;
  char user_config[512];

  // Create a new config and initialize it to nil.
  struct config config = (const struct config){0};
  struct sandbox_config sandbox_config;

  save_env();

  // Parse command-line arguments.
  if (argc == 2 && (!strcmp(argv[1], "-V") || !strcmp(argv[1], "--version"))) {
    fprintf(stderr, "%s (send bugs to <%s>)\n", PACKAGE_STRING,
            PACKAGE_BUGREPORT);
    exit(0);
  }
  if (argc < 4) {
    log_println("too few arguments");
    exit(1);
  }

  if (s2i(argv[1], &target_uid) < 0) {
    log_println("bad target_uid argument");
    exit(1);
  }
  if (s2i(argv[2], &target_gid) < 0) {
    log_println("bad target_gid argument");
    exit(1);
  }

  // In the standard apache2 suexec, cmd should not be an absolute
  // path. Worth checking anyway even if we don't care much (because
  // the later allow_cmd checks supersede it).
  cmd = argv[3];
  if (cmd[0] == '/') {
    log_printf("command must be a relative path (%s)", cmd);
    exit(1);
  }

  // Convert cmd to its real path to figure out what we want to
  // execute.
  real_cmd = realpath(cmd, NULL);
  if (!real_cmd) {
    log_printf("cannot find '%s'", cmd);
    exit(1);
  }

  // Read configuration file.
  if (read_config(SUEXEC_CONFIG, &config) < 0)
    exit(102);

  // Clean the environment.
  if (clear_env(&config) < 0)
    exit(110);

  // Check validity of arguments against configuration.
  if ((target_uid == 0) || (target_uid < config.min_uid)) {
    log_printf("cannot run as forbidden uid (%d/%s)", target_uid, cmd);
    exit(103);
  }
  if ((target_gid == 0) || (target_gid < config.min_gid)) {
    log_printf("cannot run as forbidden gid (%d/%s)", target_gid, cmd);
    exit(104);
  }

  // Read the user-specific configuration, if any.
  snprintf(user_config, sizeof(user_config), SUEXEC_CONFIG_DIR "/%d", target_uid);
  read_config(user_config, &config);

  if (check_cwd(&config) < 0)
    exit(105);

  if (check_cmd(&config, real_cmd) < 0)
    exit(105);

  // Invoke the sandbox.
  if (sandbox_config_init(&sandbox_config, target_uid, target_gid, argc - 4,
                          real_cmd, (argv + 3)) < 0)
    exit(106);

  sandbox_config.enable_capabilities = config.enable_capabilities;
  sandbox_config.enable_cgroups = config.enable_cgroups;
  sandbox_config.enable_namespaces = config.enable_namespaces;
  sandbox_config.enable_syscalls = config.enable_syscalls;
  if (config.root)
    sandbox_config.new_root_dir = config.root;

  if (sandbox_start(&sandbox_config) < 0)
    exit(107);

  exit(110);
}
