#ifndef __suexec_strlist_H
#define __suexec_strlist_H 1

typedef struct {
  int sz;
  const char **values;
} strlist;

int strlist_append(strlist *, const char *);

#endif
