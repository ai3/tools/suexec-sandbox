#define _GNU_SOURCE 1

#include "config.h"
#include "log.h"
#include "strlist.h"

#include <stdlib.h>

int strlist_append(strlist *sl, const char *s) {
  if (sl->sz == 0) {
    sl->values = (const char **)malloc(sizeof(char *));
  } else {
    sl->values = (const char **)realloc(sl->values, sizeof(char *) * (sl->sz + 1));
  }
  if (!sl->values) {
    log_println("out of memory (strlist)");
    return -1;
  }
  sl->values[sl->sz++] = s;
  return 0;
}
