#define _GNU_SOURCE 1

#include "config.h"
#include "conf.h"
#include "log.h"
#include "strlist.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define MAX_LINE_SIZE 1024

// Convert string to int, with syntax checking.
int s2i(const char *s, int *i) {
  char *endptr = NULL;
  errno = 0;
  long n = strtol(s, &endptr, 10);
  if (errno != 0)
    return -1;
  if (endptr == s)
    return -1;
  *i = (int)n;
  return 0;
}

static int s2b(char *s, int *i) {
  if (!strcmp(s, "yes")
      || !strcmp(s, "y")
      || !strcmp(s, "true")
      || !strcmp(s, "on"))
    *i = 1;
  else
    *i = 0;
  return 0;
}

static int config_add_allowed_cmd(struct config *config, char *value) {
  return strlist_append(&(config->allowed_cmds), strdup(value));
}

static int config_add_docroot(struct config *config, char *value) {
  // Check the value (it must be an absolute path).
  if (value[0] != '/') {
    log_printf("docroot '%s' is not an absolute path", value);
    return -1;
  }
  if (value[1] == '/' || value[1] == '.' || value[1] == '\0') {
    log_printf("invalid docroot '%s'", value);
    return -1;
  }

  // Strip trailing slashes.
  {
    int n = strlen(value);
    while (n > 1 && value[n - 1] == '/')
      value[n--] = '\0';
  }

  // Extend the docroots array.
  return strlist_append(&(config->docroots), strdup(value));
}

static int config_set_min_uid(struct config *config, char *value) {
  return s2i(value, &(config->min_uid));
}

static int config_set_min_gid(struct config *config, char *value) {
  return s2i(value, &(config->min_gid));
}

static char *trim_comment(char *s) {
  if (s) {
    char *p = strchr(s, '#');
    if (p)
      *p = 0;
  }
  return s;
}

static char *trim_right(char *s) {
  if (s) {
    char *p = s;
    while (*p)
      p++;
    p--;
    while (isspace(*p) && p >= s)
      *p-- = 0;
  }
  return s;
}

static int parse_assignment(char *line, char **key, char **value) {
  char *sep;

  line = trim_right(trim_comment(line));
  if (*line == '\0')
    return 0;
  sep = strchr(line, ' ');
  if (!sep)
    return -1;
  while (*sep && isspace(*sep))
    *sep++ = 0;
  *key = line;
  *value = sep;
  return 0;
}

int read_config(const char *path, struct config *config) {
  char buf[MAX_LINE_SIZE], *p;
  int r = 0, lineno = 1;
  FILE *fp = NULL;

  fp = fopen(path, "r");
  if (!fp) {
    log_printf("could not open configuration file %s", path);
    return -1;
  }

  while ((p = fgets(buf, MAX_LINE_SIZE, fp)) != NULL) {
    char *key = NULL, *value = NULL;
    if (parse_assignment(p, &key, &value) < 0) {
      log_printf("syntax error at %s:%d: not in 'attribute value' format", path,
                 lineno);
      return -1;
    }
    if (!key)
      continue;

    if (!strcmp(key, "path")) {
      config->path = strdup(value);
    } else if (!strcmp(key, "root")) {
      config->root = strdup(value);
    } else if (!strcmp(key, "allowed_cmd")) {
      r = config_add_allowed_cmd(config, value);
    } else if (!strcmp(key, "docroot")) {
      r = config_add_docroot(config, value);
    } else if (!strcmp(key, "min_uid")) {
      r = config_set_min_uid(config, value);
    } else if (!strcmp(key, "min_gid")) {
      r = config_set_min_gid(config, value);
    } else if (!strcmp(key, "enable_capabilities")) {
      r = s2b(value, &(config->enable_capabilities));
    } else if (!strcmp(key, "enable_cgroups")) {
      r = s2b(value, &(config->enable_cgroups));
    } else if (!strcmp(key, "enable_namespaces")) {
      r = s2b(value, &(config->enable_namespaces));
    } else if (!strcmp(key, "enable_syscalls")) {
      r = s2b(value, &(config->enable_syscalls));
    } else {
      log_printf("syntax error at %s:%d: unknown directive '%s'", path, lineno,
                 key);
      return -1;
    }
    if (r < 0) {
      log_printf("syntax error at %s:%d: error setting attribute", path,
                 lineno);
      return r;
    }

    lineno++;
  };

  fclose(fp);
  return 0;
}
